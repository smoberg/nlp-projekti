import nltk
from nltk import RegexpTokenizer
import os
import sys
import io
import re
import glob
import string
import warnings
from stop_words import get_stop_words
import codecs
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    from gensim import corpora, models
    from gensim.test.utils import datapath
    from gensim.models.coherencemodel import CoherenceModel
    import gensim
    from nltk.stem.snowball import SnowballStemmer
    import pyLDAvis
    import pyLDAvis.gensim



tokenizer = RegexpTokenizer(r'\w+')
stemmer = SnowballStemmer('finnish')

def create_docset(arg):
    #files = glob.glob("*.txt")
    corpus = []
    with codecs.open(arg, encoding = 'utf-8') as f:
        content = f.read()
        clean = re.sub('<a.*?a>', '', content)
        admin = re.sub('>th.*?>', '', clean)
        #tämä on täällä ihan turhaan, paitsi että lukee tiedoston listaksi, ei poista stopwordeja tms.
        final = remove_stopwords(admin)
        corpus.append(final)
    return corpus

def readWords(file):
    with open(file, 'r', encoding='utf-8') as words:
        wordList=[word.strip() for word in words]
        # print(wordList)
        return wordList

def remove_stopwords(text):

    stopwordlist=readWords('finnishST.txt')
    punctuation = list(string.punctuation)
    tokens = nltk.word_tokenize(text)
    filtered_thread = [w for w in tokens if not w in stopwordlist and not w in punctuation and len(w) > 1]

    return filtered_thread


def main():
    doc_set = []
    words = []
    cwords = []
    final = []
    doc_set = create_docset(sys.argv[1])
    finlist = readWords('finnish.txt')
    #dataset contained some english threads
    englist = get_stop_words('en')
    finlist.pop(0)
    finlist.append('aiemmin')
    #for i in finlist:
        #finalist = tokenizer.tokenize(finlist)
        #print(i)


    #print(doc_set)

    for i in doc_set[0]:
        low = i.lower()
        #a = re.sub(r"target=\S+", "", low)
        tok = tokenizer.tokenize(low)
        stopwords = [w for w in tok if not w in finlist and w not in englist]
        stopwords = [stemmer.stem(w) for w in stopwords]
        words.append(stopwords)
    print('stemmed')
    #if re.sub() didn't cath all unwanted items
    for inner_l in words:
        for item in inner_l:
            if len(item) > 3 and item.isnumeric() == False and item != 'quot' and item != 'messag' and item != 'removed':
                cwords.append(item)
    for i in cwords:
        newtok = tokenizer.tokenize(i)
        final.append(newtok)
    #print(final)
    '''
    with open(sys.argv[2], 'w', encoding = 'utf-8') as f:
        for i in final:
            for l in i:
                    f.write(l)
    '''
    print("formatted and tokenized\n")




    #doc_setistä id <-> termi kirjasto
    dictionary = corpora.Dictionary(final)

    # document-term matriisi
    matr = [dictionary.doc2bow(t) for t in final]

    #LDA malli
    print("generating lda-model\n")
    ldamodel = gensim.models.ldamodel.LdaModel(matr, num_topics=4, id2word = dictionary, passes= 15)
    print(ldamodel.print_topics(num_topics=4, num_words=8))
    #temp_file = datapath('LDAmodel')
    #ldamodel.save(temp_file)

    print('\nPerplexity: ', ldamodel.log_perplexity(matr))

    coherence_model_lda = CoherenceModel(model=ldamodel, texts=final, dictionary=dictionary, coherence='c_v')
    coherence_lda = coherence_model_lda.get_coherence()
    print('\nCoherence Score: ', coherence_lda)

    vis = pyLDAvis.gensim.prepare(ldamodel, matr, dictionary)
    pyLDAvis.save_html(vis, 'lda.html')



if __name__ == '__main__':
    main()
