# coding=utf-8
import json
import nltk
import string
import os
import sys
import regex as re
import collections
import numpy as np
import matplotlib.pyplot as plt
global LocationName
global OrganisationName
global PersonName

LocationName = "EnamexLoc"
OrganisationName = "EnamexOrg"
PersonName = "EnamexPrs"


Entity = collections.namedtuple('Entity', ['word', 'type', 'count'])

def readWords(file):
    with open(file, 'r', encoding='utf-8') as words:
        wordList=[word.strip() for word in words]
        # print(wordList)
        return wordList


def find_entities(wordlist):
    # tee lista joho kaikki named entityt tupleina
    # ota talteen sana, entityn tyyppi ja count
    # käy läpi lista aina ku lisäät uuden ja jos löytyy sama sana niin count+1
    list=[]
    for word in wordlist:
        if LocationName in word or OrganisationName in word or PersonName in word:
            tempword, temptype = word.split('\t')
            Modified = False
            if len(list) > 0:
                for i in range(len(list)):
                    if tempword == list[i].word:
                        list[i] = Entity(word=tempword, type=temptype, count=list[i].count+1)
                        Modified = True

                if Modified == False:
                    list.append(Entity(word=tempword, type=temptype, count=1))
            else:
                list.append(Entity(word=tempword, type=temptype, count=1))
                # tekee vain kerran niinku pitääki
    return list

def plothistogram(named_entity_list):
    plt.style.use('grayscale')
    entityname = []
    entityfrequency = []
    for entity in sorted(named_entity_list, key=lambda x:x.count, reverse=True):
        if OrganisationName in entity.type:
            print(entity)
            entityname.append(entity.word)
            entityfrequency.append(entity.count)
        if len(entityname) ==20:
            break

    pos = np.arange(len(entityname))
    width = 0.8
    ax = plt.axes()
    ax.set_xticks(pos)
    ax.set_xticklabels(entityname, rotation='60', fontsize=7)

    plt.bar(pos, entityfrequency, width, align='center')
    plt.show()

def main():
    asd = readWords('tagged.txt')
    results = find_entities(asd)
    plothistogram(results)


if __name__ == '__main__':
    main()
