# coding=utf-8
import json
import nltk
import string
import os
import sys
import regex as re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cycler
import itertools

global dictionary


dictionary={}

def clean_tags(body):
  reformat = re.compile('<.*?>')
  body = re.sub(reformat, '', body)
  return body

def parse_sentences(bodies):
    sentences = []
    punctuation = list(string.punctuation)
    for body in bodies:
            body = clean_tags(body)
            body = re.split("[,!?:.]+", body)
            body = [x for x in body if len(x) > 1]
            if body:
                sentences.extend(body)
    return sentences

def readWords(file):
    with open(file, 'r') as words:
        wordList=[word.strip() for word in words]
        # print(wordList)
        return wordList

def readsentence(filename):
    sentencelist=[]
    with open(filename, 'r', encoding='utf-8') as file:
        sentencelist.extend(sentence.strip() for sentence in file)
    return sentencelist

def analyse_pairs(sentence, stopwordlist):
    manualfilter=['this','has','been', 'removed', 'message','by','the','of','"']
    punctuation = list(string.punctuation)
    words=sentence.split()
    words=[x for x in words if not x in stopwordlist and not x in manualfilter and not x in punctuation and len(x)>1]
    if len(words) > 10:
        return
    else:
        kombinaatiolista=list(itertools.combinations(words, r=2))
        for word_1, word_2 in kombinaatiolista:
             str = '%s %s' % (word_1.lower(), word_2.lower())
             # print(str)
             dictionary.setdefault(str, 0)
             dictionary[str]+=1
        return


def keywithmaxval():
     v=list(dictionary.values())
     k=list(dictionary.keys())
     return k[v.index(max(v))], max(v)


def main():
    plt.style.use('grayscale')
    paragraphs=readsentence('output_fulltext.txt')
    sentences=parse_sentences(paragraphs)
    stopwordlist=readWords('finnishST.txt')
    print(stopwordlist)
    for sentence in sentences:
        # print(sentence)
        # print('\n')
        analyse_pairs(sentence, stopwordlist)
    i=0
    two_words=[]
    frequency=[]

    for key, value in sorted(dictionary.items(), key=lambda item: (item[1], item[0]), reverse=True):
        print("%s: %s" % (key, value))
        two_words.append(key)
        frequency.append(value)
        i+=1
        if i==20:
            break
    pos = np.arange(len(two_words))
    width = 0.8
    ax = plt.axes()
    ax.set_xticks(pos)
    ax.set_xticklabels(two_words, rotation='60', fontsize=5)

    plt.bar(pos, frequency, width, align='center')
    plt.show()

if __name__ == '__main__':
    main()
