# coding=utf-8
import sys

def main():
    with open(sys.argv[3], 'w') as outfile:
        for i in range(1, int(sys.argv[2])):
            with open(sys.argv[1] + str(i) + '.txt') as inputfile:
                for line in inputfile:
                    outfile.write(line)

if __name__ == '__main__':
    main()
