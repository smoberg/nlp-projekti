# coding=utf-8
import json
import nltk
import string
import os
import sys


def read(path): #lukee json filen
    with open(path, 'r') as json_file:
        obj = json.load(json_file, encoding='utf-8')
    return obj

def readWords(file):
    with open(file, 'r') as words:
        wordList=[word.strip() for word in words]
        # print(wordList)
        return wordList

def writeJson(json_obj):
    if not os.path.isfile(str(sys.argv[2])):
        a=[]
        a.append(json_obj)
        with open(str(sys.argv[2]), mode='w') as f:
            f.write(json.dumps(a, indent=2))
    else:
        with open(str(sys.argv[2])) as datafile:
            data = json.load(datafile)

        data.append(json_obj)
        with open(str(sys.argv[2]), mode='w') as f:
            f.write(json.dumps(data, indent=2))

def main():
    json_obj = read(str(sys.argv[1]))
    securityWords = readWords('securityWords.txt')
    for obj in json_obj:
        text = obj["body"]
        words = remove_stopwords(text)
        if compare(securityWords, words) == True:
            writeJson(obj)

    # text = json_obj[0]["body"] # valittee alotuspostauksen tekstin
    # print(text)
    # words = remove_stopwords(text)
    # # words = ['asd', 'asdtietoturva']
    #
    # if compare(securityWords, words) == True:
    #     writeJson(json_obj[0])




def compare(security_words, words):
    # Vertaa tekstistä löytyviä sanoja securitysanoihin
    # palauttaa True tai False riippuen löytyykö threadista security sanoja
    matches=0
    for security_word in security_words:
        for word in words:
            if security_word in word:
                matches=matches+1
    # print(matches)
    if matches > 5:
        return True
    else:
        return False


def remove_stopwords(text):
    # poistaa stopwordit, random symbolit ja tokenisoi tekstin listaksi sanoja
    # palauttaa listan sanoja
    # tähä voi lainaa todennäkösesti koodia jota käytettiin SNA kurssilla
    stopwordlist=readWords('finnishST.txt')
    punctuation = list(string.punctuation)
    tokens = nltk.word_tokenize(text)
    filtered_thread = [w for w in tokens if not w in stopwordlist and not w in punctuation and len(w) > 1]

    # print(len(tokens))
    # print(len(filtered_thread))
    # print(filtered_thread)

    return filtered_thread

if __name__ == '__main__':
    main()
