# coding=utf-8
import sys

def main():
  splitLen = int(sys.argv[2])
  outputBase = sys.argv[3]

  f = open(sys.argv[1], 'r').read().split('\n')

  at = 1
  for lines in range(0, len(f), splitLen):
      outputData = f[lines:lines+splitLen]

      output = open(outputBase + str(at) + '.txt', 'w')
      output.write('\n'.join(outputData))
      output.close()

      at += 1

if __name__ == '__main__':
  main()
