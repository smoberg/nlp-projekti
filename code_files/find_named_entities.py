# coding=utf-8
import json
import nltk
import string
import os
import sys
import regex as re

def read(path): #lukee json filen
    with open(path, 'r') as json_file:
        obj = json.load(json_file, encoding='utf-8')
    return obj

def find_all_values_by_key(data, key):
    results = []
    for var in data:
        if key in var:
            results.append(var[key])
        if "comments" in var:
            nested = find_nested(var["comments"], key)
            results.extend(nested)
    return results

def find_nested(data, key):
    results = []
    for var in data:
         results.append(var[key])
    return results


def readWords(file):
    with open(file, 'r') as words:
        wordList=[word.strip() for word in words]
        # print(wordList)
        return wordList

def remove_stopwords(text, stopwordlist):
    # poistaa stopwordit, random symbolit ja tokenisoi tekstin listaksi sanoja
    # palauttaa listan sanoja
    # tähä voi lainaa todennäkösesti koodia jota käytettiin SNA kurssilla

    punctuation = list(string.punctuation)
    tokens = nltk.word_tokenize(text)
    filtered_thread = [w for w in tokens if not w in stopwordlist and not w in punctuation and len(w) > 1]
    filtered_thread = [''.join(x for x in par if x not in string.punctuation) for par in filtered_thread]
    # print(len(tokens))
    # print(len(filtered_thread))
    # print(filtered_thread)

    return filtered_thread

def main():
    wordlist=[]
    stopwordlist=readWords('finnishST.txt')
    # print(json.dumps(json_obj[0], indent=4, sort_keys=True))
    json_obj = read('data.json')

    for obj in json_obj:
        bodies = find_all_values_by_key(obj["comments"], "body")
        bodies.append(obj["body"])
        for body in bodies:
            wordlist.extend(remove_stopwords(body, stopwordlist))



    with open('output.txt', 'w', encoding='utf8') as f:
        for word in wordlist:
            f.write("%s\n" % word)


    # print(text)


if __name__ == '__main__':
    main()
