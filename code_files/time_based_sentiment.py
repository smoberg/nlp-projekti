#coding=utf-8
import nltk
import sys
import re
import json
from googletrans import Translator
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import string
import pandas as pd
import string
from afinn import Afinn


def read(path): #lukee json filen
    with open(path, 'r') as json_file:
        obj = json.load(json_file, encoding='utf-8')
    return obj


def translate(sentences):
    translator = Translator();
    translated = translator.translate(sentences)
    return sentences


def clean_tags(body):
  reformat = re.compile('<.*?>')
  body = re.sub(reformat, '', body)
  return body

def parse_sentences(bodies):
    sentences = []
    punctuation = list(string.punctuation)

    for body in bodies:

            body = clean_tags(body)
            body = re.split("[,!?:.]+", body)
            body = [x for x in body if len(x) > 1]

            if body:
                sentences.extend(body)

    return sentences

def find_all_values_by_key(data, key):
    results = []
    for var in data:
        if key in var:
            results.append(var[key])
        if "comments" in var:
            nested = find_nested(var["comments"], key)
            results.extend(nested)
    return results

def find_nested(data, key):
    results = []
    for var in data:
         results.append(var[key])
    return results

def sentimentsAfinn(sentences):
    afinn = Afinn(language="fi")
    result = {"positive":0,"neutral":0,"negative":0}
    for sentence in sentences:
        score = afinn.score(sentence)
        if score == 0.0:
            result["neutral"] +=1
        elif score > 0.0:
            result["positive"] +=1
        else:
            result["negative"] +=1
    return result

def main():
    jsonData = read(str(sys.argv[1]))

    bodies = []
    sentences = []

    for obj in jsonData:
        timestamp = obj["created_at"]
        if timestamp >= 978307200 and timestamp/1000 < 1009843200:
            first = obj["body"]
            bodies.extend(find_all_values_by_key(obj["comments"], "body"))

    sentences.extend(parse_sentences(bodies))

    results = sentimentsAfinn(sentences)
    print(results)

if __name__ == '__main__':
    main()
